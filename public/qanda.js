/* global SentenceProvider, LanguageSwitcher, MultipleChoice, SprayReader */

class Qanda {
  constructor() {
    this.domRoot = undefined;
    this.domElements = {};
    
    let language = window.location.pathname.slice(1, 3);
    if(!Object.values(LanguageSwitcher.LANGUAGES).includes(language)) language = undefined;
    
    // this.languageSwitcher = new LanguageSwitcher(this.localeWasChanged.bind(this), language);
    this.sentenceProvider = new SentenceProvider(this);
    this.createDom();
    this.insertDom();
    
    this.translate();
    
    this.points = 0;
    
    this.sprayReader = new SprayReader('#spray_result');
    this.sprayReader.afterDoneCallback = this.hideSpeedReading.bind(this);
    
    this.domElements.repeatSpeedReadingButton.addEventListener('click', ()=> {
      this.sprayReader.wordIdx = 0;
      this.sprayReader.stop();
      this.startSpeedReading();
    });
  }
  
  localeWasChanged() {
    this.nextQuestion();
    this.translate();
  }
  
  translate() {
    document.querySelectorAll("[data-translation]").forEach((element) => {
      element.innerText = Qanda.LOCALE[this.currentLanguageIdentifier()][element.dataset.translation];
    });
  }
  
  currentLanguageIdentifier() {
    return 'en';
    // return this.languageSwitcher.currentLanguageIdentifier();
  }
  
  createDom() {
    this.domRoot = document.createElement('div');
    this.domRoot.classList.add('blacked-out-sentence');
    this.domElements.title = document.createElement('h4');
    this.domElements.paragraph = document.createElement('p');
    this.domElements.sentenceBefore = document.createElement('span');
    this.domElements.blackedOutWordInputContainer = document.createElement('div');
    this.domElements.blackedOutWordInputContainer.classList.add('blacked-out-input-container');
    this.domElements.sentenceAfter = document.createElement('span');
    
    this.domElements.repeatSpeedReadingButton = document.createElement('button');
      this.domElements.repeatSpeedReadingButton.innerText = "Repeat speed reading";
      this.domElements.repeatSpeedReadingButton.classList.add('margin-top-5px');
      this.domElements.repeatSpeedReadingButton.classList.add('hidden');

    this.domRoot.appendChild(this.domElements.title);
    this.domElements.paragraph.appendChild(this.domElements.sentenceBefore);
    this.domElements.paragraph.appendChild(this.domElements.blackedOutWordInputContainer);
    this.domElements.paragraph.appendChild(this.domElements.sentenceAfter);
    this.domRoot.appendChild(this.domElements.paragraph);
  }
  
  submitHandler(e) {
    if(e.type === 'keydown' && e.which !== 13) return;
    this.checkAnswer();
    this.nextQuestion();
  }
  
  checkAnswer(correctAnswer) {
    if(correctAnswer) {
      this.points++;
      this.flash(Qanda.LOCALE[this.currentLanguageIdentifier()]['correctAnswer'](this.points));
    } else {
      this.points = 0;
      this.flash(Qanda.LOCALE[this.currentLanguageIdentifier()]['wrongAnswer']('<b>' + this.blackedOutWord + '</b>'));
    }
  }
  
  flash(message) {
    let flash = document.getElementById('flash');
    flash.innerHTML = message;
    flash.classList.remove('hidden');
  }
  
  nextQuestion() {
    this.domElements.repeatSpeedReadingButton.classList.add('hidden');

    this.domRoot.parentNode.classList.add('loading')
    this.stopSpeedReading();

    this.loadQuestion((sentence)=> {

      this.domRoot.parentNode.classList.remove('loading');
      this.currentMultipleChoice.focus();
      
      if((sentence.sentence.before.length + sentence.sentence.after.length) > 140) {
        this.domElements.repeatSpeedReadingButton.classList.remove('hidden');

        this.configureSpeedReading(sentence.sentence.before + " " + sentence.sentence.after);
        this.startSpeedReading();
      }
    });
  }
  
  configureSpeedReading(questionWithoutBlackedOutWork) {
    this.sprayReader.setInput(questionWithoutBlackedOutWork);
    this.sprayReader.setWpm(500);
  }
  
  hideSpeedReading() {
    $("#spray_container").addClass('hidden');
  }
  
  stopSpeedReading() {
    this.hideSpeedReading();
    this.sprayReader.stop();
  }
  
  startSpeedReading() {
    this.sprayReader.start();
    $("#spray_container").removeClass('hidden');
  }
  
  getDom() {
    return this.domRoot;
  }
  
  getRepeatSpeedReadingButton() {
    return this.domElements.repeatSpeedReadingButton;
  }
  
  loadQuestion(callback) {
    this.sentenceProvider.get().then((sentence)=> {
      this.currentMultipleChoice = new MultipleChoice(sentence.choices, this.answerProvided.bind(this));
      this.replaceBlackedOutInput(this.currentMultipleChoice.getDom());
      this.domElements.title.innerText = sentence.articleTitle;
      this.domElements.sentenceBefore.innerText = sentence.sentence.before + " ";
      this.domElements.sentenceAfter.innerText = " " + sentence.sentence.after;
      this.blackedOutWord = sentence.sentence.blackedOutWord;
      callback(sentence);
    });
  }
  
  answerProvided(isCorrectAnswer) {
    this.checkAnswer(isCorrectAnswer);
    this.nextQuestion();
  }
  
  replaceBlackedOutInput(inputDom) {
    this.domElements.blackedOutWordInputContainer.innerHTML = '';
    this.domElements.blackedOutWordInputContainer.appendChild(inputDom);
  }
  
  insertDom() {
    let main = document.getElementsByTagName('main')[0];
    main.appendChild(this.getRepeatSpeedReadingButton());
    main.appendChild(this.getDom());
  }
}

Qanda.LOCALE = {
  en: {
    explanation: "Guess the word that is missing from the sentence.",
    correctAnswer: (points) => { return `Correct answer, ${points} points` },
    wrongAnswer: (blackedOutWord) => { return `Wrong answer, correct answer would've been ${blackedOutWord}` },
    skipHint: 'Press Enter or press "Check" to skip'
  }
}

Object.values(LanguageSwitcher.LANGUAGES).forEach(function(languageIdentifier) {
  Qanda.LOCALE[languageIdentifier] = Qanda.LOCALE.en;
});

Qanda.LOCALE.de = {
  explanation: "Errate das das fehlende Wort im Satz.",
  correctAnswer: (points) => { return `Richtige Antwort, ${points} Punkte` },
  wrongAnswer: (blackedOutWord) => { return `Falsche Antwort, die richtige Antwort wäre ${blackedOutWord} gewesen` },
  skipHint: 'Drücke Enter oder klicke/tappe auf "Überprüfen" um zu überspringen'
}


let qanda = new Qanda();
qanda.nextQuestion();