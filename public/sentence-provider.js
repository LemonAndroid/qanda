/* global axios */
const qandaEndpoint = "https://koma.lib.id/qanda-api@dev/";

class SentenceProvider {
  constructor(qanda) {
    this.qanda = qanda;
  }
  
  async get() {
    let languageIdentifier = this.qanda.currentLanguageIdentifier();
    
    let params = {language: languageIdentifier};
    
    return axios.get(qandaEndpoint, {params: params})
    .then((response) => {
      let questionData = response.data;
      this.redactCorrectWord(questionData);
      return questionData;
    }).catch((error) => {
      
      if(error.response === undefined || error.response.status === 403) {
        console.warn(error);
        return this.get();
      }
    });
  }
  
  redactCorrectWord(questionData) {
    let redactedMap = [questionData.articleTitle, questionData.sentence.before, questionData.sentence.after].map((text) => {
      let correctAnswerWordRegexp = new RegExp('/W' + questionData.sentence.blackedOutWord + '/W', 'ig');
      return text.replace(correctAnswerWordRegexp, "<redacted correct answer>");
    });
    
    questionData.articleTitle = redactedMap[0];
    questionData.sentence.before = redactedMap[1];
    questionData.sentence.after = redactedMap[2];    
  }
}